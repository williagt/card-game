package no.ntnu.edu.idatt2001.williagt.oblig5.cardgame;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class CardGraphicalInterface extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        //Instantiating objects I'll need later
        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards();

        stage.setTitle("Card Game");

        //Root and nodes
        BorderPane root = new BorderPane();

        VBox left = new VBox(10);{
            left.alignmentProperty().setValue(Pos.CENTER);
        }
        HBox center = new HBox();{
            center.alignmentProperty().setValue(Pos.CENTER);
        }
        VBox right = new VBox(10);{
            right.alignmentProperty().setValue(Pos.CENTER_LEFT);
        }

        root.setLeft(left);
        root.setCenter(center);
        root.setRight(right);

        //Image of the deck of cards
        FileInputStream deckFileInputStream = new FileInputStream("images/playing-cards.jpg");
        Image deckImage = new Image(deckFileInputStream);
        ImageView deckImageView = new ImageView(deckImage);
        deckImageView.setFitHeight(300);
        deckImageView.setFitWidth(300);
        left.getChildren().add(deckImageView);

        //Text for the hand
        Text handText = new Text(hand.toString());
        center.getChildren().add(handText);

        //Button that deals cards to hand
        Button dealHandButton = new Button("Deal hand");
        dealHandButton.setOnAction(s -> {
            hand.getNewHand(deck.dealHand(5));
            handText.setText(hand.toString());
        });
        left.getChildren().add(dealHandButton);

        //Button that shows hand
        Button showHand = new Button("Show hand");
        showHand.setOnAction(s -> {
            handText.setText(hand.toString());
        });
        right.getChildren().add(showHand);

        //Button that checks the hand for 5 card flush
        Button checkFlushButton = new Button("Check for flush");
        checkFlushButton.setOnAction(s -> {
            String hasFlushWith;
            switch(hand.has5CardFlush()){
                case 'S':
                    hasFlushWith = "You have flush with Spades"; break;
                case 'D':
                    hasFlushWith = "You have flush with Diamonds"; break;
                case 'C':
                    hasFlushWith = "You have flush with Clubs"; break;
                case 'H':
                    hasFlushWith = "You have flush with Hearts"; break;
                default:
                    hasFlushWith = "You do not have flush"; break;
            }
            handText.setText(hasFlushWith);
        });
        right.getChildren().add(checkFlushButton);

        //Button that computes sum of card values on hand
        Button computeSumOfCardValuesButton = new Button("Compute sum of card values");
        computeSumOfCardValuesButton.setOnAction(s -> {
            handText.setText(String.valueOf(hand.sumOfAllCards()));
        });
        right.getChildren().add(computeSumOfCardValuesButton);

        //Button that shows all cards of hearts on your hand
        Button allHearstButton = new Button("Show all cards of Hearts");
        allHearstButton.setOnAction(s -> {
            if(hand.allCardsOfHearts().isEmpty()){
                handText.setText("There are no hearts on your hand");
            }else{
                handText.setText(hand.allCardsOfHearts());
            }
        });
        right.getChildren().add(allHearstButton);

        //Button that checks if the Queen of spades is on your hand
        Button containsQueenButton = new Button("Check for Queen of Spades");
        containsQueenButton.setOnAction(s -> {
            if(hand.containsQueenOfSpades()){
                handText.setText("You have the Queen of Spades on your hand");
            }else{
                handText.setText("You do not have the Queen of Spades on your hand");
            }
        });
        right.getChildren().add(containsQueenButton);


        Scene scene1 = new Scene(root);
        stage.setScene(scene1);

        stage.setWidth(1000);
        stage.setHeight(600);
        stage.show();
    }

    public static void main(String [] args){
        launch(args);
    }
}
