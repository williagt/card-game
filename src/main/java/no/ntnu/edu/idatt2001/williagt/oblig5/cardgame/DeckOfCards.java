package no.ntnu.edu.idatt2001.williagt.oblig5.cardgame;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {

    private ArrayList<PlayingCard> deck = new ArrayList<>();
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private final Random random = new Random();

    public DeckOfCards(){
        for(char suit : suit){
            for(int i = 1; i <= 13; i++){
                deck.add(new PlayingCard(suit, i));
            }
        }
    }

    /**
     * Method for drawing a new hand. The amountOfDrawnCards must be an integer between 1-52, or else
     * an IllegalArgumentException will be thrown. The same cards could potentially be drawn more than once.
     *
     * @param amountOfDrawnCards the amount of cards you would like to draw from the deck
     * @return an ArrayList containing a new hand of cards
     */
    public ArrayList<PlayingCard> dealHand(int amountOfDrawnCards){
        if(amountOfDrawnCards < 1 || amountOfDrawnCards > 52){
            throw new IllegalArgumentException("You must pick an amount between 1 and 52.");
        }

        ArrayList<PlayingCard> drawnCards = new ArrayList<>();
        for(int i = 0; i < amountOfDrawnCards; i++){
            drawnCards.add(deck.get(random.nextInt(deck.size()))); //TODO: maybe do something about the possibility for drawing the same card more than once
        }

        return drawnCards;
    }

    public ArrayList<PlayingCard> getDeckOfCards(){
        return deck;
    }
}
