package no.ntnu.edu.idatt2001.williagt.oblig5.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {

    private ArrayList<PlayingCard> hand = new ArrayList<>();
    private final char[] suit = {'S', 'H', 'D', 'C'};

    /**
     * Method for getting a new hand of cards. If drawnCards has a size of five, they will be the new hand and the method returns true.
     * Else the method will not do anything with the hand, and return false.
     *
     * @param drawnCards cards that are supposed to become the new hand
     * @return a boolean based on how many cards are drawn
     */
    public boolean getNewHand(ArrayList<PlayingCard> drawnCards){
        boolean addFiveCards = drawnCards.size() == 5;
        if(addFiveCards){
            hand = drawnCards;
        }
        return addFiveCards;
    }

    /**
     * Method for checking if you have a 5 card flush on your hand.
     *
     * Different chars will be returned based on which suit you got a flush with.
     * If there is no flush, the char X is returned.
     *
     * @return S for flush with spades, H for flush with hearts, D for flush with diamonds or C for clubs.
     *         If neither of these are returned, X is returned.
     */
    public char has5CardFlush(){
        for(char suit : suit){
            if(countCardsWithSuit(suit) == 5){
                return suit;
            }
        }
        return 'X';
    }

    private int countCardsWithSuit(char suit){ //Helper method for has5CardFlush
        long count = hand.stream()
                .filter(s -> s.getSuit() == suit)
                .count();
        return (int) count;
    }

    public int sumOfAllCards(){
        int sum = hand.stream()
                .mapToInt(s -> s.getFace())
                .sum();
        return sum;
    }

    public String allCardsOfHearts(){
        List<PlayingCard> hearts = hand.stream().filter(s -> s.getSuit() == 'H').collect(Collectors.toList());
        String heartsString = "";
        for(PlayingCard p : hearts){
            heartsString += "'" + p.getAsString() + "' ";
        }
        return heartsString;
    }

    public boolean containsQueenOfSpades(){
        List<PlayingCard> queen = hand.stream().filter(s -> s.getSuit() == 'S' && s.getFace() == 12).collect(Collectors.toList());
        return queen.size() == 1;
    }

    public String toString(){
        String playersHand = "";

        for(PlayingCard card : hand){
            playersHand += "'" + card.getAsString() + "' ";
        }

        return playersHand;
    }
}
