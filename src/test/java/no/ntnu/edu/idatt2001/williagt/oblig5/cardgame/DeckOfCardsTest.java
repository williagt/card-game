package no.ntnu.edu.idatt2001.williagt.oblig5.cardgame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    private DeckOfCards deck;

    @BeforeEach
    public void init(){
        deck = new DeckOfCards();
    }

    @Test
    public void instantiateDeckOfCardsAndPutting52CardsInTheDeck(){
        Assertions.assertEquals(52, deck.getDeckOfCards().size());
    }

    @Nested
    public class DealHandTests{

        @Test
        public void dealNAmountOFCardsAndThusDeckContainsNLessCards(){
            ArrayList<PlayingCard> newHand = deck.dealHand(7); //Does this break AAA principle since I arrange and act at the same time? :thinking:

            Assertions.assertEquals(7, newHand.size());
        }

        @Test
        public void dealLessCardsThan1(){
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
               deck.dealHand(0);
            });
        }

        @Test
        public void dealMoreCardsThan52(){
            Assertions.assertThrows(IllegalArgumentException.class, () -> {
                deck.dealHand(53);
            });
        }
    }
}