package no.ntnu.edu.idatt2001.williagt.oblig5.cardgame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class HandOfCardsTest { //TODO: Add more tests for HandOfCards

    @Nested
    public class Has5CardFlushTests{

        @Test
        public void has5Spades(){
            ArrayList<PlayingCard> drawnCards = new ArrayList<>();
            drawnCards.add(new PlayingCard('S', 1));
            drawnCards.add(new PlayingCard('S', 2));
            drawnCards.add(new PlayingCard('S', 3));
            drawnCards.add(new PlayingCard('S', 4));
            drawnCards.add(new PlayingCard('S', 5));

            HandOfCards hand = new HandOfCards();
            hand.getNewHand(drawnCards);

            Assertions.assertEquals('S', hand.has5CardFlush());
        }

        @Test
        public void has4SpadesAndOneHeart(){
            ArrayList<PlayingCard> drawnCards = new ArrayList<>();
            drawnCards.add(new PlayingCard('S', 1));
            drawnCards.add(new PlayingCard('S', 2));
            drawnCards.add(new PlayingCard('S', 3));
            drawnCards.add(new PlayingCard('S', 4));
            drawnCards.add(new PlayingCard('H', 5));

            HandOfCards hand = new HandOfCards();
            hand.getNewHand(drawnCards);

            Assertions.assertEquals('X', hand.has5CardFlush());
        }
    }

}